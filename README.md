﻿# Crew Chief plugin for Richard Burns Rally
This plugin writes data extracted via RBRAPI to shared memory.  That data is used by the Crew Chief app to provide co-driver functionality. 

# Acknowledgements
This plugin would not have been possible without all the people who worked on improving RBR before me for the past 17 years.  All of You made this game playable in 2020, and this just another humble contribution.

Special thanks go to:
* WorkerBee - for reverse engineering and documenting Pacenote IDs used by RBR.
* Mika-N - for embracing open source and cooperative spirit and sharing known offsets into binary.  Also, for teaching me nice trick on synchronizing on EndScene and for patience with my questions.
* Wotever - for sharing offsets and math in interpreting RBR orientation matrix and for SimHub.
* RBRPro team - for putting together one-click-to-VR package.
* Jim Britton - for supporting this effort with great ideas, insights and fantastic acting.
* Morten Roslev - for being my hacking Sensei and great CCMC teammate.

# Author
Vytautas Leonavičius

# Support (kinda)
thecrewchief.org

# Installation
Latest version is included with the Crew Chief, and can be found at: `Program Files (x86)\Britton IT Ltd\CrewChiefV4\plugins\RBR\`