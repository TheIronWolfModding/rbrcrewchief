﻿/*
Author: The Iron Wolf (vleonavicius@hotmail.com)
Website: thecrewchief.org
*/
#include "pch.h"

#include "api/RBRAPI.h"
#include "CrewChiefPlugin.h"
#include "Utils.h"
//#include "MappedBuffer.h"
#include "RBRData.h"

long CrewChiefPlugin::msDebugOutputLevel = static_cast<long>(DebugLevel::Off);
static_assert(sizeof(long) <= sizeof(DebugLevel), "sizeof(long) <= sizeof(DebugLevel)");

long CrewChiefPlugin::msDebugOutputSource = static_cast<long>(DebugSource::General);
static_assert(sizeof(long) <= sizeof(DebugSource), "sizeof(long) <= sizeof(DebugSource)");

long CrewChiefPlugin::msDebugReadRateMs = 0L; // Full speed

FILE* CrewChiefPlugin::msDebugFile = nullptr;

CrewChiefPlugin::CrewChiefPlugin(IRBRGame* pGame)
  : mGame(*pGame)
  , mPerFrameData(CrewChiefPlugin::MM_MAPPED_PER_FRAME_DATA_FILE_NAME)
  , mCoDriverData(CrewChiefPlugin::MM_MAPPED_CODRIVER_DATA_FILE_NAME)
  , mExtended(CrewChiefPlugin::MM_MAPPED_EXTENDED_FILE_NAME)
{
  LoadConfig();

  DEBUG_MSG(DebugLevel::Timing, DebugSource::Overrides, "invoked");
  Initialize();
}

CrewChiefPlugin::~CrewChiefPlugin()
{
  DEBUG_MSG(DebugLevel::CriticalInfo, DebugSource::General, "Shutting down");

  mEnabled = false;

  if (msDebugFile != nullptr) {
    fclose(msDebugFile);
    msDebugFile = nullptr;
  }
}

char const*
CrewChiefPlugin::GetName()
{
  DEBUG_MSG(DebugLevel::DevInfo, DebugSource::Overrides, "invoked");

  // Workaround for a conflict with the Gauger plugin which also tries to hook into EndScene.
  // We can't init detours during plugin creation because of that.
  static auto once = false;
  if (!once) {
    once = true;

    mDetourD3DEndScene.reset(new DetourXS(reinterpret_cast<LPVOID>(0x0040E890), ::DetourRBRDirectXEndScene, TRUE));
    mpfRBRDirectXEndScene = static_cast<tRBRDirectXEndScene>(mDetourD3DEndScene->GetTrampoline());
    DEBUG_MSG(DebugLevel::CriticalInfo, DebugSource::General, "Detours initialized.");
  }

  return "Crew Chief";
}

void
CrewChiefPlugin::DrawFrontEndPage()
{
  mGame.SetColor(0.0f, 0.0f, 0.0f, 0.5f);
  mGame.DrawFlatBox(0.0f, 0.0f, 380.0f, 480.0f);
  
  mGame.SetMenuColor(IRBRGame::MENU_TEXT);
  mGame.SetFont(IRBRGame::FONT_BIG);
  mGame.WriteText(10.0f, 50.0f, PLUGIN_NAME_AND_VERSION "  - by Vytautas Leonavicius");
  mGame.WriteText(10.0f, 70.0f, "Support and suggestions at: http://thecrewchief.org");
  mGame.WriteText(10.0f, 110.0f, "CCMC(tm) 2020.  Everything is prohibited.");
  mGame.WriteText(10.0f, 150.0f, "GitLab: https://gitlab.com/TheIronWolfModding/rbrcrewchief");

  DEBUG_MSG(DebugLevel::DevInfo, DebugSource::Overrides, "invoked");
}

void
CrewChiefPlugin::DrawResultsUI()
{
  DEBUG_MSG(DebugLevel::DevInfo, DebugSource::Overrides, "invoked");
}

void
CrewChiefPlugin::HandleFrontEndEvents(char, bool, bool, bool, bool, bool)
{
  DEBUG_MSG(DebugLevel::DevInfo, DebugSource::Overrides, "invoked");
}

void
CrewChiefPlugin::TickFrontEndPage(float)
{
  DEBUG_MSG(DebugLevel::DevInfo, DebugSource::Overrides, "invoked");
}

void
CrewChiefPlugin::StageStarted(int, char const*, bool)
{
  DEBUG_MSG(DebugLevel::DevInfo, DebugSource::Overrides, "invoked");
}

void
CrewChiefPlugin::HandleResults(float, float, float, char const*)
{
  DEBUG_MSG(DebugLevel::DevInfo, DebugSource::Overrides, "invoked");
}

void
CrewChiefPlugin::CheckPoint(float, int, char const*)
{
  DEBUG_MSG(DebugLevel::DevInfo, DebugSource::Overrides, "invoked");
}

void
CrewChiefPlugin::WriteDebugMsg(DebugLevel lvl,
                               long src,
                               char const* const functionName,
                               int line,
                               char const* const msg,
                               ...)
{
  if (Utils::IsFlagOff(CrewChiefPlugin::msDebugOutputLevel, lvl)
      || Utils::IsFlagOff(CrewChiefPlugin::msDebugOutputSource, src))
    return;

  va_list argList;
  if (CrewChiefPlugin::msDebugFile == nullptr) {
    CrewChiefPlugin::msDebugFile = _fsopen(CrewChiefPlugin::DEBUG_OUTPUT_FILENAME, "a", _SH_DENYNO);
    setvbuf(CrewChiefPlugin::msDebugFile, nullptr, _IOFBF, CrewChiefPlugin::BUFFER_IO_BYTES);
  }

  SYSTEMTIME st = {};
  ::GetLocalTime(&st);

  fprintf_s(CrewChiefPlugin::msDebugFile,
            "%.2d:%.2d:%.2d.%.3d TID:0x%04lx  ",
            st.wHour,
            st.wMinute,
            st.wSecond,
            st.wMilliseconds,
            ::GetCurrentThreadId());
  fprintf_s(CrewChiefPlugin::msDebugFile, "%s(%d) : ", functionName, line);

  if (lvl == DebugLevel::Errors)
    fprintf_s(CrewChiefPlugin::msDebugFile, "ERROR: ");
  else if (lvl == DebugLevel::Warnings)
    fprintf_s(CrewChiefPlugin::msDebugFile, "WARNING: ");

  if (CrewChiefPlugin::msDebugFile != nullptr) {
    va_start(argList, msg);
    vfprintf_s(CrewChiefPlugin::msDebugFile, msg, argList);
    va_end(argList);
  }

  fprintf_s(CrewChiefPlugin::msDebugFile, "\n");

  // Flush periodically for low volume messages.
  static ULONGLONG lastFlushTicks = 0uLL;
  auto const ticksNow = GetTickCount64();
  if ((ticksNow - lastFlushTicks) / MILLISECONDS_IN_SECOND > DEBUG_IO_FLUSH_PERIOD_SECS) {
    fflush(CrewChiefPlugin::msDebugFile);
    lastFlushTicks = ticksNow;
  }
}

void
CrewChiefPlugin::TraceLastWin32Error()
{
  if (Utils::IsFlagOn(CrewChiefPlugin::msDebugOutputLevel, DebugLevel::Errors))
    return;

  auto const lastError = ::GetLastError();
  if (lastError == 0)
    return;

  LPSTR messageBuffer = nullptr;
  auto const retChars
    = ::FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                       nullptr /*lpSource*/,
                       lastError,
                       MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                       reinterpret_cast<LPSTR>(&messageBuffer),
                       0 /*nSize*/,
                       nullptr /*argunments*/);

  DEBUG_MSG(DebugLevel::Errors, DebugSource::General, "Win32 error code: %d", lastError);

  if (retChars > 0 && messageBuffer != nullptr)
    DEBUG_MSG(DebugLevel::Errors, DebugSource::General, "Win32 error description: %s", messageBuffer);

  ::LocalFree(messageBuffer);
}

CrewChiefPlugin* gCCP = nullptr;

HRESULT
__fastcall DetourRBRDirectXEndScene(void* objPointer)
{
  assert(gCCP != nullptr);
  gCCP->OnD3DEndScene(objPointer);
  return gCCP->mpfRBRDirectXEndScene(objPointer);
}

void
CrewChiefPlugin::Initialize()
{
  assert(gCCP == nullptr);
  gCCP = this;

  if (CrewChiefPlugin::msDebugReadRateMs > 0L)
    DEBUG_MSG(
      DebugLevel::Warnings, DebugSource::General, "refresh rate reduced to %dms", CrewChiefPlugin::msDebugReadRateMs);

    // Print out configuration.
#ifdef VERSION_AVX2
#ifdef VERSION_MT
  DEBUG_MSG(DebugLevel::CriticalInfo,
            DebugSource::General,
            "Starting Crew Chief Plugin Version: %s",
            SHARED_MEMORY_VERSION " AVX2+PGO+MT");
#else
  DEBUG_MSG(DebugLevel::CriticalInfo,
            DebugSource::General,
            "Starting Crew Chief Plugin Version: %s",
            SHARED_MEMORY_VERSION " AVX2+PGO");
#endif
#else
  DEBUG_MSG(
    DebugLevel::CriticalInfo, DebugSource::General, "Starting Crew Chief Plugin Version: %s", SHARED_MEMORY_VERSION);
#endif
  DEBUG_MSG(DebugLevel::CriticalInfo, DebugSource::General, "Configuration:");
  DEBUG_MSG(
    DebugLevel::CriticalInfo, DebugSource::General, "debugOutputLevel: %ld", CrewChiefPlugin::msDebugOutputLevel);
  DEBUG_MSG(
    DebugLevel::CriticalInfo, DebugSource::General, "debugOutputSource: %ld", CrewChiefPlugin::msDebugOutputSource);
  DEBUG_MSG(DebugLevel::CriticalInfo, DebugSource::General, "debugReadRateMs: %ld", CrewChiefPlugin::msDebugReadRateMs);

  RETURN_IF_FALSE(InitMappedBuffer(mPerFrameData, "PerFrameData"));
  RETURN_IF_FALSE(InitMappedBuffer(mCoDriverData, "CoDriver"));
  RETURN_IF_FALSE(InitMappedBuffer(mExtended, "Extended"));

  static_assert(sizeof(mExtended.mpWriteBuff->mVersion) >= sizeof(SHARED_MEMORY_VERSION),
                "Invalid plugin version string (too long).");

  mExtended.BeginUpdate();
  ::strcpy_s(mExtended.mpWriteBuff->mVersion, SHARED_MEMORY_VERSION);
  mExtended.EndUpdate();

  mEnabled = true;
}

void
CrewChiefPlugin::OnD3DEndScene(void* objPointer)
{
  UNREFERENCED_PARAMETER(objPointer);

  if (!mEnabled)
    return;

  if (CrewChiefPlugin::msDebugReadRateMs > 0L) {
    static auto ticksPrev = 0uLL;
    auto const ticksNow = GetTickCount64();
    if (ticksNow - ticksPrev < CrewChiefPlugin::msDebugReadRateMs)
      return;

    ticksPrev = ticksNow;
  }

  __try {
    // Re-read pointers.
    RBRAPI_InitializeObjReferences();
    RBRAPI_InitializeRaceTimeObjReferences();

    if (g_pRBRCarInfo != nullptr && g_pRBRGameMode != nullptr) {
      static auto stageStartCountdownPrev = -1.0f;
      static __int32 gameModePrev = -1;
      auto const stageCountdownNow = g_pRBRCarInfo->stageStartCountdown;
      auto const gameModeNow = g_pRBRGameMode->gameMode;

      // ~144FPS update rate.
      if (stageCountdownNow == stageStartCountdownPrev && gameModeNow == gameModePrev) {

        DEBUG_MSG(DebugLevel::Timing,
                  DebugSource::DataRead,
                  "Skipping data refresh due to no updates.  Countdown: %f  GameMode: %d",
                  stageCountdownNow,
                  gameModeNow);
        return;
      }

      stageStartCountdownPrev = stageCountdownNow;
      gameModePrev = gameModeNow;
    }
  } __except (::GetExceptionCode() == EXCEPTION_ACCESS_VIOLATION ? EXCEPTION_EXECUTE_HANDLER
                                                                 : EXCEPTION_CONTINUE_SEARCH) {
    DEBUG_MSG(DebugLevel::Errors, DebugSource::General, "Exception during memory pre-read.");
    return;
  }

  auto shouldReloadPacenotes = false;

  DEBUG_MSG(DebugLevel::Timing, DebugSource::DataRead, "Begin data read");
  mPerFrameData.BeginUpdate();
  __try {
    auto const prevFrameCountdown = mPerFrameData.mpWriteBuff->mRBRCarInfo.stageStartCountdown;
    memset(mPerFrameData.mpWriteBuff, 0, sizeof(std::remove_pointer<decltype(mPerFrameData.mpWriteBuff)>::type));

    if (g_pRBRGameMode != nullptr) {
      memcpy(&(mPerFrameData.mpWriteBuff->mRBRGameMode),
             g_pRBRGameMode,
             sizeof(std::remove_pointer<decltype(g_pRBRGameMode)>::type));

      DEBUG_MSG(DebugLevel::Timing,
                DebugSource::DataRead,
                "got GameMode.  GameMode: %d",
                mPerFrameData.mpWriteBuff->mRBRGameMode.gameMode);
    } else
      DEBUG_MSG(DebugLevel::Timing, DebugSource::DataRead, "GameMode missing");

    WritePerFrameData(g_pRBRGameModeExt, mPerFrameData.mpWriteBuff->mRBRGameModeExt, "GameDataExt");

    if (g_pRBRCarInfo != nullptr) {
      memcpy(&(mPerFrameData.mpWriteBuff->mRBRCarInfo),
             g_pRBRCarInfo,
             sizeof(std::remove_pointer<decltype(g_pRBRCarInfo)>::type));

      DEBUG_MSG(DebugLevel::Timing,
                DebugSource::DataRead,
                "got CarInfo.  RaceTime: %f",
                mPerFrameData.mpWriteBuff->mRBRCarInfo.raceTime);

      if (mPerFrameData.mpWriteBuff->mRBRCarInfo.stageStartCountdown < 6.999f
          && Utils::IsEqual(prevFrameCountdown, 7.0f)) {
        DEBUG_MSG(DebugLevel::DevInfo,
                  DebugSource::DataRead,
                  "Resetting pacenotes.  StageCountdown: %f",
                  mPerFrameData.mpWriteBuff->mRBRCarInfo.stageStartCountdown);

        shouldReloadPacenotes = true;
      }
    } else
      DEBUG_MSG(DebugLevel::Timing, DebugSource::DataRead, "CarInfo missing");

    WritePerFrameData(g_pRBRCarControls, mPerFrameData.mpWriteBuff->mRBRCarControls, "CarControls");
    WritePerFrameData(g_pRBRMapSettings, mPerFrameData.mpWriteBuff->mRBRMapSettings, "MapSettings");
    WritePerFrameData(g_pRBRCarMovement, mPerFrameData.mpWriteBuff->mRBRCarMovement, "CarMovement");

    if (g_pRBRMapInfo != nullptr) {
      mPerFrameData.mpWriteBuff->stageLength = g_pRBRMapInfo->stageLength;
      DEBUG_MSG(DebugLevel::Timing, DebugSource::DataRead, "got MapInfo");
    } else
      DEBUG_MSG(DebugLevel::Timing, DebugSource::DataRead, "MapInfo missing");

    wcscpy_s(mPerFrameData.mpWriteBuff->currentLocationStringWide, g_currentLocationString);

    mPerFrameData.EndUpdate();
  } __except (::GetExceptionCode() == EXCEPTION_ACCESS_VIOLATION ? EXCEPTION_EXECUTE_HANDLER
                                                                 : EXCEPTION_CONTINUE_SEARCH) {
    DEBUG_MSG(DebugLevel::Errors, DebugSource::DataRead, "Exception while reading memory.");

    memset(mPerFrameData.mpWriteBuff, 0, sizeof(std::remove_pointer<decltype(mPerFrameData.mpWriteBuff)>::type));

    mPerFrameData.EndUpdate();
  }

  // Read the pacenotes once per countdown.
  if (shouldReloadPacenotes) {
    if (g_pRBRPacenotes != nullptr && g_pRBRCarInfo != nullptr) {
      mCoDriverData.BeginUpdate();
      __try {
        memcpy(&(mCoDriverData.mpWriteBuff->mRBRPacenoteInfo),
               g_pRBRPacenotes,
               sizeof(std::remove_pointer<decltype(g_pRBRPacenotes)>::type));

        if (mCoDriverData.mpWriteBuff->mRBRPacenoteInfo.numPacenotes > 0) {
          memcpy(&(mCoDriverData.mpWriteBuff->mPacenotes),
                 g_pRBRPacenotes->pPacenotes,
                 sizeof(RBRPacenote) * mCoDriverData.mpWriteBuff->mRBRPacenoteInfo.numPacenotes);
        } else if (mCoDriverData.mpWriteBuff->mRBRPacenoteInfo.numPacenotes > RBRCoDriverData::MAX_PACENOTES_SUPPORTED)
          DEBUG_MSG(DebugLevel::Errors, DebugSource::General, "Number of max allowed pacenotes exceeded.");
        else {
          assert(false);
          DEBUG_MSG(DebugLevel::Errors, DebugSource::General, "No pacenotes detected.");
        }

        DEBUG_MSG(DebugLevel::DevInfo,
                  DebugSource::DataRead,
                  "Pacenotes reloaded:  StageCountdown: %f",
                  mPerFrameData.mpWriteBuff->mRBRCarInfo.stageStartCountdown);

        mCoDriverData.EndUpdate();
      } __except (::GetExceptionCode() == EXCEPTION_ACCESS_VIOLATION ? EXCEPTION_EXECUTE_HANDLER
                                                                     : EXCEPTION_CONTINUE_SEARCH) {
        DEBUG_MSG(DebugLevel::Errors, DebugSource::DataRead, "Exception while reading pacenotes.");

        mCoDriverData.EndUpdate();
      }
    } else {
      DEBUG_MSG(DebugLevel::DevInfo,
                DebugSource::DataRead,
                "Pacenotes reload request ignored: StageCountdown: %f",
                mPerFrameData.mpWriteBuff->mRBRCarInfo.stageStartCountdown);
    }
  }

  DEBUG_MSG(DebugLevel::Timing, DebugSource::DataRead, "End data read");

  return;
}

template<typename T>
void
CrewChiefPlugin::WritePerFrameData(T* pGameData, T& outputDataBuffer, char const* const buffName)
{
  if (pGameData != nullptr) {
    memcpy(&outputDataBuffer, pGameData, sizeof(T));

    DEBUG_MSG(DebugLevel::Timing, DebugSource::DataRead, "got %s", buffName);
  } else
    DEBUG_MSG(DebugLevel::Timing, DebugSource::DataRead, "%s missing", buffName);
}

template<typename BuffT>
bool
CrewChiefPlugin::InitMappedBuffer(BuffT& buffer, char const* const buffLogicalName)
{
  if (!buffer.Initialize(false /*mapGlobally*/)) {
    DEBUG_MSG(DebugLevel::Errors, DebugSource::General, "Failed to initialize %s mapping", buffLogicalName);
    return false;
  }

  auto const size
    = static_cast<int>(sizeof(BuffT::BufferType) + sizeof(typename MappedBuffer<BuffT>::MappedBufferVersionBlock));
  DEBUG_MSG(DebugLevel::CriticalInfo, DebugSource::General, "Size of the %s buffer: %d bytes.", buffLogicalName, size);

  return true;
}

void
CrewChiefPlugin::LoadConfig()
{
  char wd[MAX_PATH] = {};
  ::GetCurrentDirectory(MAX_PATH, wd);

  auto iniPath = ::lstrcatA(wd, CrewChiefPlugin::CONFIG_FILE_REL_PATH);

  auto outputLvl = ::GetPrivateProfileInt("config", "debugOutputLevel", 0, iniPath);
  auto sanitized = min(max(outputLvl, static_cast<long>(DebugLevel::CriticalInfo)), static_cast<long>(DebugLevel::All));
  CrewChiefPlugin::msDebugOutputLevel = sanitized;

  if (CrewChiefPlugin::msDebugOutputLevel != static_cast<long>(DebugLevel::Off))
    remove(CrewChiefPlugin::DEBUG_OUTPUT_FILENAME);

  auto outputSrc = ::GetPrivateProfileInt("config", "debugOutputSource", 0, iniPath);
  sanitized = min(max(outputSrc, static_cast<long>(DebugSource::General)), static_cast<long>(DebugSource::All));
  CrewChiefPlugin::msDebugOutputSource = sanitized;

  auto readRateMs = static_cast<int>(::GetPrivateProfileInt("config", "debugReadRateMs", 0, iniPath));
  sanitized = std::abs(readRateMs);
  CrewChiefPlugin::msDebugReadRateMs = sanitized;

  auto const attr = ::GetFileAttributes(iniPath);

  if (attr == INVALID_FILE_ATTRIBUTES || attr & FILE_ATTRIBUTE_DIRECTORY) 
    DEBUG_MSG(DebugLevel::CriticalInfo, DebugSource::General, "Configuration file: %s not found, defaults loaded", iniPath);
  else
    DEBUG_MSG(DebugLevel::CriticalInfo, DebugSource::General, "Loaded config from: %s", iniPath);
}
