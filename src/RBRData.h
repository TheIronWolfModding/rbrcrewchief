/*
Definition of mapped RBR structures and related types.

Author: The Iron Wolf (vleonavicius@hotmail.com)
Website: thecrewchief.org
*/
#pragma once
#include "api/RBRAPI.h"

struct RBRMappedBufferHeader
{};

struct RBRPerFrameData : public RBRMappedBufferHeader
{
  RBRGameMode mRBRGameMode;
  RBRGameModeExt mRBRGameModeExt;
  RBRCarInfo mRBRCarInfo;
  RBRCarControls mRBRCarControls;
  RBRMapSettings mRBRMapSettings;
  RRBRCarMovement mRBRCarMovement;

  // From RBRMapInfo
  __int32 stageLength;   // 0x75310  Length of current stage (to the time checking station few meters after the finish line. RBRCarInfo.stageProgress value is related to this total length) ?
  wchar_t currentLocationStringWide[1024];

#if 0 // Don't care.
  RBRGameConfig mRBRGameConfig;
  RBRCameraInfo mRBRCameraInfo;
  extern __int32*             mRBRGhostCarReplayMode; // 0=No ghost, 1=Saving ghost car movements (during racing), 2=Replying ghost car movements (0x892EEC)
  RBRGhostCarMovement mRBRGhostCarMovement;
  RBRMenuSystem mRBRMenuSystem;
  // ext2 seems to be empty
  RBRGameModeExt2 mRBRGameModeExt2;
#endif
};

struct RBRCoDriverData : public RBRMappedBufferHeader
{
  static int constexpr MAX_PACENOTES_SUPPORTED = 4096;

  RBRPacenotes mRBRPacenoteInfo;
  RBRPacenote mPacenotes[RBRCoDriverData::MAX_PACENOTES_SUPPORTED];
};

struct RBRExtended : public RBRMappedBufferHeader
{
  char mVersion[12];                           // API version
};