/*
Author: The Iron Wolf (vleonavicius@hotmail.com)
Website: thecrewchief.org
*/
#pragma once
#include "RBRData.h"

// Each component can be in [0:99] range.
// Note: each time major version changes, that means layout has changed, and clients might need an update.
#define PLUGIN_VERSION_MAJOR "1.3"
#define PLUGIN_VERSION_MINOR "0.0"

#ifdef VERSION_AVX2
#ifdef VERSION_MT
#define PLUGIN_NAME_AND_VERSION "Crew Chief plugin v" PLUGIN_VERSION_MAJOR " AVX2+PGO+MT"
#else
#define PLUGIN_NAME_AND_VERSION "Crew Chief plugin v" PLUGIN_VERSION_MAJOR " AVX2+PGO"
#endif
#else
#define PLUGIN_NAME_AND_VERSION "Crew Chief plugin v" PLUGIN_VERSION_MAJOR "." PLUGIN_VERSION_MINOR
#endif

#define SHARED_MEMORY_VERSION PLUGIN_VERSION_MAJOR "." PLUGIN_VERSION_MINOR

#define DEBUG_MSG(lvl, src, msg, ...) CrewChiefPlugin::WriteDebugMsg(lvl, src, __FUNCTION__, __LINE__, msg, __VA_ARGS__)
#define RETURN_IF_FALSE(expression)                                                                                    \
  if (!(expression)) {                                                                                                 \
    DEBUG_MSG(DebugLevel::Errors, DebugSource::General, "Operation failed");                                           \
    return;                                                                                                            \
  }

// Needs DEBUG_MSG definition.
#include "MappedBuffer.h"

enum class DebugLevel : long
{
  Off = 0,
  Errors = 1,
  CriticalInfo = 2,
  DevInfo = 4,
  Warnings = 8,
  Synchronization = 16,
  Perf = 32,
  Timing = 64,
  Verbose = 128,
  All = 255,
};

enum DebugSource : long
{
  General = 1, // CriticalInfo, Error and Warning level messages go there as well as some core messages.
  Overrides = 2,
  MappedBufferSource = 4,
  DataRead = 8,
  All = 15,
};

static double constexpr MILLISECONDS_IN_SECOND = 1000.0;
static double constexpr MICROSECONDS_IN_MILLISECOND = 1000.0;
static double constexpr MICROSECONDS_IN_SECOND = MILLISECONDS_IN_SECOND * MICROSECONDS_IN_MILLISECOND;

class CrewChiefPlugin : public IPlugin
{
  friend HRESULT __fastcall DetourRBRDirectXEndScene(void* objPointer);
  
public:
  static long msDebugOutputLevel;
  static long msDebugOutputSource;
  static long msDebugReadRateMs;

public:
  // Debug output helpers
  static void WriteDebugMsg(DebugLevel lvl,
                            long src,
                            char const* const functionName,
                            int line,
                            char const* const msg,
                            ...);

  static void TraceLastWin32Error();

public:
  CrewChiefPlugin(IRBRGame* pGame);
  virtual ~CrewChiefPlugin();

  // Inherited via IPlugin
  char const* GetName() override;
  void DrawFrontEndPage() override;
  void DrawResultsUI() override;

  void HandleFrontEndEvents(char txtKeyboard, bool bUp, bool bDown, bool bLeft, bool bRight, bool bSelect) override;

  void TickFrontEndPage(float fTimeDelta) override;

  void StageStarted(int iMap, char const* ptxtPlayerName, bool bWasFalseStart) override;

  void HandleResults(float fCheckPoint1, float fCheckPoint2, float fFinishTime, const char* ptxtPlayerName) override;

  void CheckPoint(float fCheckPointTime, int iCheckPointID, char const* ptxtPlayerName) override;

  // Helpers.
  void OnD3DEndScene(void* objPointer);

private:
  static char constexpr* const CONFIG_FILE_REL_PATH = R"(\plugins\CrewChief.ini)";  // Relative to RBR root

  static char constexpr* const DEBUG_OUTPUT_FILENAME = R"(CC_DebugOutput.txt)";
  static int constexpr BUFFER_IO_BYTES = 2048;
  static int constexpr DEBUG_IO_FLUSH_PERIOD_SECS = 10;

  static char constexpr* const MM_MAPPED_PER_FRAME_DATA_FILE_NAME = "$RBRCC_PerFrameData$";
  static char constexpr* const MM_MAPPED_CODRIVER_DATA_FILE_NAME = "$RBRCC_CoDriverData$";
  static char constexpr* const MM_MAPPED_EXTENDED_FILE_NAME = "$RBRCC_Extended$";

  static FILE* msDebugFile;

private:
  void Initialize();

  template<typename BuffT>
  bool InitMappedBuffer(BuffT& buffer, char const* const buffLogicalName);

  template <typename T>
  void WritePerFrameData(T* pGameData, T& outputDataBuffer, char const* const buffName);

  void LoadConfig();

private:
  IRBRGame& mGame;
  bool mEnabled = false;

  std::unique_ptr<DetourXS> mDetourD3DEndScene;
  tRBRDirectXEndScene mpfRBRDirectXEndScene = nullptr;

  MappedBuffer<RBRPerFrameData> mPerFrameData;
  MappedBuffer<RBRCoDriverData> mCoDriverData;
  MappedBuffer<RBRExtended> mExtended;
};