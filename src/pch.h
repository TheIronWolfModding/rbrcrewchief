// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once


#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

#include <stdio.h>
#include <assert.h>
#include <string>
#include <memory>
#include <mutex>
#include <utility>

// You need to define this before the includes, or use /D __SUPPORT_PLUGIN__=1 
// in the C/C++ / Commandline project configuration
#define __SUPPORT_PLUGIN__ 1

#include "api/IPlugin.h"
#include "api/IRBRGame.h"
#include "external/detourxs/detourxs.h"

#define CC_API_CHANGE 1  // Indicates differences from Mika's published API
