/*
Author: The Iron Wolf (vleonavicius@hotmail.com)
Website: thecrewchief.org
*/
#include "pch.h"
#include "api/rbrapi.h"
#include "crewchiefplugin.h"

class IRBRGame;

CrewChiefPlugin* gpPlugin = nullptr;

BOOL APIENTRY
DllMain(
  HANDLE hModule,
  DWORD ul_reason_for_call,
  LPVOID lpReserved)
{
  UNREFERENCED_PARAMETER(hModule);
  UNREFERENCED_PARAMETER(ul_reason_for_call);
  UNREFERENCED_PARAMETER(lpReserved);
  return TRUE;
}

IPlugin*
RBR_CreatePlugin(
  IRBRGame* pGame)
{
  if (gpPlugin == nullptr)
    gpPlugin = new CrewChiefPlugin(pGame);

  return gpPlugin;
}